# Changelog

## Version 0.1.0


Initial implementation of full communication (OK/NEXT/ERRor), Experiment
files, Machine connection interface. Adaptation of QSConnectionAsync to
use new communication. Move of monitor system into qslib.

## Version 0.0.0 / etc


Initial version of QSConnectionAsync and other low-level portions to
support rawquant and qpcr\_data\_updater.
