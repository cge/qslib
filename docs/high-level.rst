.. _high-level:

High-level API: Experiment, Protocol, PlateSetup, Protocol
==========================================================

Class for accessing and manipulating experiments
--------------------------------------------------

.. autoclass:: qslib.common.Experiment
   :members:
   :undoc-members:
   :noindex:

Classes for defining experiments
--------------------------------

.. autoclass:: qslib.common.Protocol
    :members:
    :undoc-members:
    :noindex:

.. autoclass:: qslib.common.Stage
    :members:
    :undoc-members:
    :noindex:

.. autoclass:: qslib.common.Step
    :members:
    :undoc-members:
    :noindex:

.. autoclass:: qslib.common.PlateSetup
    :members:
    :undoc-members:
    :noindex:

    
Classes for interacting with machines
-------------------------------------

.. autoclass:: qslib.common.Machine
   :members:
   :undoc-members:
   :noindex:

.. autoclass:: qslib.common.RunStatus
    :members:
    :undoc-members:
    :noindex:

.. autoclass:: qslib.common.MachineStatus
    :members:
    :undoc-members:
    :noindex:

